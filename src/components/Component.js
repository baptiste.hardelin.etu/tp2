class Component {
	tagName;
	attribute;
	children;
	constructor(tagName, attribute, children) {
		this.tagName = tagName;
		this.attribute = attribute;
		this.children = children;
	}

	render() {
		if (this.attribute != null) {
			return this.renderAttribute();
		}
		this.renderChildren();
		return `<${this.tagName}>${this.children}</${this.tagName}>`;
	}
	renderAttribute() {
		if (this.children == '') {
			return `<${this.tagName} ${this.attribute.name}="${this.attribute.value}" />`;
		} else {
			this.renderChildren();
			return `<${this.tagName} ${this.attribute.name}="${this.attribute.value}">${this.children}</${this.tagName}>`;
		}
	}

	renderChildren() {
		if (this.children != null && this.children instanceof Array) {
			let res = '';
			for (let i = 0; i < this.children.length; i++) {
				if (this.children[i] instanceof Component) {
					res += this.children[i].render();
				} else {
					res += this.children[i];
				}
			}
			this.children = res;
		}
	}
}

export default Component;