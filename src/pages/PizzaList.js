import Component from "../components/Component.js";
import PizzaThumbnail from "../components/PizzaThumbnail.js";
import data from "../data.js";

class PizzaList extends Component {
    #pizzas;
    constructor(pizzas) {
        super('section', {name:'class', value:'pizzaList'}, pizzas.map(e => new PizzaThumbnail(e)));
        this.pizzas = pizzas;
    }

    set pizzas(value) {
        super.children = value.map(e => new PizzaThumbnail(e));
    }

}


export default PizzaList;