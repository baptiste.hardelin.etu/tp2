import Component from "./components/Component";

class Router {
    static titleElement;
    static contentElement;
    static routes;
    
    static navigate(path) {
        let pathElement = this.routes.find(e => e.path = path);
        this.titleElement.innerHTML = new Component('h1', null, pathElement.title).render();
        this.contentElement.innerHTML = pathElement.page.render();
    }
}

export default Router;