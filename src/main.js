import data from './data.js';
import Component from './components/Component.js';
import Img from './components/Img.js';
import PizzaThumbnail from './components/PizzaThumbnail.js';
import PizzaList from './pages/PizzaList.js';
import Router from './Router.js';

// B2.1, B2.2
const title = new Component('h1', null, 'La Carte');
document.querySelector('.pageTitle').innerHTML = title.render();
// B2.3, B2.4
const img = new Img("https://images.unsplash.com/photo-1532246420286-127bcd803104?fit=crop&w=500&h=300");
// B4.1
document.querySelector('.pageContent').innerHTML = img.render();
// D1.1
/*const title2 = new Component( 'h1', null, ['La', ' ', 'carte'] );
document.querySelector('.pageTitle').innerHTML = title2.render();*/
// D1.2
const c = new Component(
	'article',
	{name:'class', value:'pizzaThumbnail'},
	[
		new Img('https://images.unsplash.com/photo-1532246420286-127bcd803104?fit=crop&w=500&h=300'),
		'Regina'
	]
);
document.querySelector( '.pageContent' ).innerHTML = c.render();

// D1.3
const pizza = data[0];
const pizzaThumbnail = new PizzaThumbnail(pizza);
document.querySelector( '.pageContent' ).innerHTML = pizzaThumbnail.render();

// D2.1
/*const pizzaList = new PizzaList(data);
document.querySelector( '.pageContent' ).innerHTML = pizzaList.render();

// D3.2.2
Router.titleElement = document.querySelector('.pageTitle');
Router.contentElement = document.querySelector('.pageContent');
Router.routes = [
	{path: '/', page: pizzaList, title: 'La carte'}
];
Router.navigate('/'); // affiche 'La carte' dans .pageTitle et la pizzaList dans .pageContent*/

// D4.3
const pizzaList = new PizzaList([]);

Router.titleElement = document.querySelector('.pageTitle');
Router.contentElement = document.querySelector('.pageContent');
Router.routes = [{ path: '/', page: pizzaList, title: 'La carte' }];

Router.navigate('/'); // affiche une page vide
pizzaList.pizzas = data;
Router.navigate('/'); // affiche la liste des pizzas