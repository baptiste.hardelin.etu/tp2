<img src="images/readme/header.jpg">

## Objectifs
- Savoir faire de la POO en ES6
- Mettre en oeuvre le système de modules
- Et faire évoluer notre application ***"Pizzaland"*** 🍕

## Sommaire
Pour plus de clarté, les instructions du TP se trouvent dans des fichiers distincts (un fichier par sujet), procédez dans l'ordre sinon, ça fonctionnera beaucoup moins bien ! :

1. [A. Préparatifs](A-preparatifs.md)
2. [B. La POO](B-poo.md)
3. [C. Modules](C-modules.md)
4. [D. POO avancée](D-poo-avancee.md)


# Avant de continuer lancer les commandes

    npx serve -l 8000

    npm run watch
    